// console.log("Hello World");
/*

3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
a. Name (String)
b. Age (Number)
c. Pokemon (Array)
d. Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.

*/

let pokemonTrainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock", "Misty"]
	}
	
	};
	pokemonTrainer.talk = function(talk){
		console.log("Pikachu! I choose you!");
	}
console.log(pokemonTrainer);
console.log("Result of dot notation:");
console.log(pokemonTrainer.name);
console.log("Result of square bracket notation:");
console.log(pokemonTrainer["pokemon"]);
console.log("Result of talk method:");
pokemonTrainer.talk();